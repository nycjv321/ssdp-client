pub fn extract(line: &str, header: &str) -> Option<String> {
    if line.to_lowercase().starts_with(header.to_lowercase().as_str()) {
        Some(line.to_lowercase().replace(header.to_lowercase().as_str(), ""))
    } else {
        None
    }
}
