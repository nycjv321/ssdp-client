extern crate clap;

use clap::{Arg, App, ArgMatches};
use std::process::exit;

mod ssdp;

use ssdp::{ResponseHeaders};

fn args() -> ArgMatches<'static> {
    return App::new("ssdp-client")
        .version("1.0")
        .author("Javier L. Velasquez <nycjv321@gmail.com>")
        .about("A simple SSDP client")
        .arg(Arg::with_name("server_pattern")
            .long("server")
            .value_name("SERVER")
            .help("a server pattern to look for")
            .takes_value(true))
        .arg(Arg::with_name("print_payload")
            .long("print-payload")
            .value_name("PRINT PAYLOAD")
            .help("providing this param instructs this application to print the human form of the SSDP M-SEARCH request")
            .takes_value(false))
        .get_matches();
}

fn main() {
    let resolved_args = &args();


    if resolved_args.is_present("print_payload") {
        let payload_lines = ssdp::SSDP_SEARCH_PAYLOAD.split("\n");
        println!();
        for payload_line in payload_lines {
            println!("\t{}", payload_line)
        }
        println!();
    } else {
        let print_headers = |response_headers: &ResponseHeaders| {
            fn determine_server(abc: &ArgMatches) -> String {
                match abc.value_of("server_pattern") {
                    Some(server) => { return String::from(server) }
                    None => {
                        eprintln!("did not provide valid --server_pattern");
                        exit(1)
                    }
                };
            }
            let resolved_server = determine_server(resolved_args);
            if response_headers.server.contains(resolved_server.as_str()) {
                println!("{{\"server\" : \"{}\", \"location\": \"{}\"}}", response_headers.server, response_headers.location);
                exit(0);
            }
        };
        ssdp::listen( print_headers)
    }
}

